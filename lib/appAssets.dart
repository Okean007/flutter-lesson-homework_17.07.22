// ignore_for_file: file_names

abstract class AppAssets {
  static const images = _Images();
  static const svg = _Svg();
}

class _Images {
  const _Images();
  final String episods = 'assets/images/bitmap/episods.png';
  final String settings = 'assets/images/bitmap/settings.png';
  final String persons = 'assets/images/bitmap/persons.png';
  final String world = 'assets/images/bitmap/world.png';
}

class _Svg {
  const _Svg();
  final String episode = 'assets/images/svg/episode.svg';
  final String location = 'assets/images/svg/location.svg';
  final String subtract = 'assets/images/svg/subtract.svg';
}
