import 'package:bottomnavigatbar/appAssets.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

List details = [
  Widgets(
    title: 'Персонажи',
    image: AppAssets.images.persons,
    textColor: Colors.black,
    appColor: Colors.black,
  ),
  Widgets(
    appColor: Colors.blueGrey,
    title: 'Локации',
    image: AppAssets.images.world,
    textColor: Colors.blueGrey,
  ),
  Widgets(
    appColor: Colors.brown,
    title: 'Эпизоды',
    image: AppAssets.images.episods,
    textColor: Colors.brown,
  ),
  Widgets(
    appColor: Colors.grey,
    title: 'Настройки',
    image: AppAssets.images.settings,
    textColor: Colors.grey,
  )
];

class BottomNavigator extends StatefulWidget {
  const BottomNavigator({Key? key}) : super(key: key);

  @override
  State<BottomNavigator> createState() => _BottomNavigatorState();
}

class _BottomNavigatorState extends State<BottomNavigator> {
  int counter = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: details[counter],
      bottomNavigationBar: BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
          currentIndex: counter,
          unselectedItemColor: Colors.grey,
          selectedItemColor: Colors.blue,
          selectedFontSize: 15.0,
          unselectedFontSize: 15.0,
          items: [
            BottomNavigationBarItem(
              icon: SvgPicture.asset(
                AppAssets.svg.subtract,
                color: Colors.grey,
              ),
              activeIcon: SvgPicture.asset(
                AppAssets.svg.subtract,
                color: Colors.blue,
              ),
              label: 'Персонажи',
            ),
            BottomNavigationBarItem(
              icon: SvgPicture.asset(
                AppAssets.svg.location,
              ),
              activeIcon: SvgPicture.asset(
                AppAssets.svg.location,
                color: Colors.blue,
              ),
              label: 'Локации',
            ),
            BottomNavigationBarItem(
              icon: SvgPicture.asset(
                AppAssets.svg.episode,
              ),
              activeIcon: SvgPicture.asset(
                AppAssets.svg.episode,
                color: Colors.blue,
              ),
              label: 'Эписоды',
            ),
            const BottomNavigationBarItem(
              icon: Icon(Icons.settings_outlined),
              label: 'Настройки',
            ),
          ],
          onTap: (index) {
            counter = index;
            setState(() {});
          }),
    );
  }
}

class Widgets extends StatelessWidget {
  const Widgets(
      {Key? key,
      required this.title,
      required this.image,
      required this.textColor,
      required this.appColor})
      : super(key: key);
  final String title;
  final String image;
  final Color textColor;
  final Color appColor;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: appColor,
        title: const Text('BottomNavigationBar'),
        elevation: 10,
      ),
      body: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
        Text(
          title,
          style: TextStyle(
            fontSize: 50,
            fontWeight: FontWeight.w900,
            color: textColor,
          ),
        ),
        Expanded(
          child: Image.asset(
            image,
          ),
        ),
      ]),
    );
  }
}
